const bcrypt = require('bcryptjs');

const truncate = require('../utils/truncate');
const { User } = require('../../src/app/models');

describe('Authentication', () => {
  beforeEach(async () => {
    await truncate();
  });
  it('should encrypt user password', async () => {
    const user = await User.create({
      name: 'Rodrigo',
      email: 'rlv@rlv',
      password: '123456',
    });

    const compareHash = await bcrypt.compare('123456', user.password_hash);

    expect(compareHash).toBe(true);
  });
});
